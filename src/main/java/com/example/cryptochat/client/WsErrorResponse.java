package com.example.cryptochat.client;

import lombok.Data;

@Data
public class WsErrorResponse extends WsTypedMessage {

    public static final String TYPE = "ERROR";

    public WsErrorResponse() {
        this.type = TYPE;
    }

    public WsErrorResponse(String message) {
        this();
        this.message = message;
    }

    private String message;

}
