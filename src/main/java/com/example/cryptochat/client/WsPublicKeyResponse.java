package com.example.cryptochat.client;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WsPublicKeyResponse extends WsTypedMessage {

    public static final String TYPE = "PUBLIC_KEY_RESPONSE";

    public WsPublicKeyResponse() {
        this.type = TYPE;
    }

    public WsPublicKeyResponse(String messageId, String user, String publicKey) {
        this();
        this.messageId = messageId;
        this.user = user;
        this.publicKey = publicKey;
    }

    private String messageId;

    private String user;

    private String publicKey;

}
