package com.example.cryptochat.client;

import lombok.Data;

import java.util.List;

@Data
public class WsUsersUpdateResponse extends WsTypedMessage {

    public static final String TYPE = "UPDATE";

    public WsUsersUpdateResponse() {
        this.type = TYPE;
    }

    public WsUsersUpdateResponse(List<String> users) {
        this();
        this.users = users;
    }

    private List<String> users;

}
