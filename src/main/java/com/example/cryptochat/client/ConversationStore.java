package com.example.cryptochat.client;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.abstractj.kalium.keys.PublicKey;

import java.util.*;

@Slf4j
public class ConversationStore {

    private Map<String, Conversation> userConversations = new HashMap<>();

    public boolean exists(String user) {
        return userConversations.containsKey(user);
    }

    public void begin(String user, PublicKey publicKey, String tag) {
        if (userConversations.containsKey(user)) {
            log.warn("Conversation already exists - skipping initialization");
        } else {
            userConversations.put(user, new Conversation(user, publicKey, tag));
        }
    }

    public void send(String user, ConversationMessage message) {
        userConversations.get(user).getMessages().add(message);
    }

    public void receive(String user, ConversationMessage message) {
        userConversations.get(user).getMessages().add(message);
    }

    public PublicKey getPublicKey(String user) {
        return userConversations.get(user).getUserPublicKey();
    }

    public String getTag(String user) {
        return userConversations.get(user).getTag();
    }

    public List<ConversationMessage> getMessages(String user) {
        return Collections.unmodifiableList(userConversations.get(user).getMessages());
    }

    @Data
    @RequiredArgsConstructor
    private class Conversation {

        private final String user;

        private final PublicKey userPublicKey;

        private final String tag;

        private final List<ConversationMessage> messages = new ArrayList<>();

    }

}


