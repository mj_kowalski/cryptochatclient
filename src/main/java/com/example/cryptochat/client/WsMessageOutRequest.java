package com.example.cryptochat.client;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class WsMessageOutRequest extends WsTypedMessage {

    public static final String TYPE = "MESSAGE_OUT";

    public WsMessageOutRequest() {
        this.type = TYPE;
    }

    public WsMessageOutRequest(String messageId, Date date, String to, String messageContent) {
        this();
        this.messageId = messageId;
        this.date = date;
        this.to = to;
        this.messageContent = messageContent;
    }

    private String messageId;

    private Date date;

    private String to;

    private String messageContent;

}
