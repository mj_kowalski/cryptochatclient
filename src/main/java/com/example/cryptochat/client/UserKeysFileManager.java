package com.example.cryptochat.client;

import lombok.RequiredArgsConstructor;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

@RequiredArgsConstructor
public class UserKeysFileManager {

    private final CryptoService cryptoService;

    public boolean exists(String user) {
        final String cleanUserName = cleanUserName(user);
        return new File(buildRelativePath(cleanUserName)).exists();
    }

    public CryptoChatKeys read(String user, String key) {
        final String cleanUserName = cleanUserName(user);
        Optional<String> line;
        try {
            line = Files.lines(Paths.get(buildRelativePath(cleanUserName))).findFirst();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        if (!line.isPresent()) {
            throw new IllegalStateException("Illegal keys file format");
        }
        String decryptedLine = cryptoService.decryptSymmetric(key, line.get());
        String[] split = decryptedLine.split("\\|");
        return cryptoService.readKeys(split[0], split[1]);
    }

    public CryptoChatKeys create(String user, String key) {
        CryptoChatKeys keys = cryptoService.generateKeys();
        String content = keys.getPrivateKey().toString() + "|" + keys.getPublicKey().toString();

        final String cleanUserName = cleanUserName(user);
        String encryptedContent = cryptoService.encryptSymmetric(key, content);
        try {
            Files.write(Paths.get(buildRelativePath(cleanUserName)), encryptedContent.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return keys;
    }

    private String buildRelativePath(String user) {
        return "./" + user + "_cryptochat.keys";
    }

    private String cleanUserName(String user) {
        return user.replaceAll("[^a-zA-Z0-9.-]", "_");
    }

}
