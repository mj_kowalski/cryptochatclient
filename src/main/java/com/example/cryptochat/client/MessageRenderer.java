package com.example.cryptochat.client;

import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Slf4j
public class MessageRenderer {

    public List<Text> render(ConversationMessage message, String myUsername) {
        log.debug("Rendering message: {} for {}", message, myUsername);
        if (message == null) {
            throw new IllegalArgumentException("Invalid (empty) message");
        }
        if (myUsername.equals(message.getFrom())) {
            return renderOutgoing(message);
        } else {
            return renderIncoming(message);
        }
    }

    private List<Text> renderIncoming(ConversationMessage message) {
        Text from = new Text(formatDate(message.getDate()) + " | " + message.getFrom() + "\t ");
        from.setFill(Color.BLUE);
        Text content = new Text(message.getContent() + "\n");
        return Arrays.asList(from, content);
    }

    private List<Text> renderOutgoing(ConversationMessage message) {
        Text from = new Text(formatDate(message.getDate()) + " | me\t ");
        from.setFill(Color.RED);
        Text content = new Text(message.getContent() + "\n");
        return Arrays.asList(from, content);
    }

    private String formatDate(Date date) {
        return new SimpleDateFormat("MM-dd HH:mm:ss").format(date);
    }

}
