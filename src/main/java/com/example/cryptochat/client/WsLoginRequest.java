package com.example.cryptochat.client;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WsLoginRequest extends WsTypedMessage {

    public static final String TYPE = "LOGIN";

    public WsLoginRequest() {
        this.type = TYPE;
    }

    public WsLoginRequest(String user, String publicKey) {
        this();
        this.user = user;
        this.publicKey = publicKey;
    }

    private String user;

    private String publicKey;

}
