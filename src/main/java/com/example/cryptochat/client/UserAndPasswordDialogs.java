package com.example.cryptochat.client;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UserAndPasswordDialogs {

    public static void show(Stage parentStage, UserKeysFileConsumer consumer) {
        Platform.runLater(() -> {
            Stage dialog = new Stage();
            dialog.initOwner(parentStage);
            dialog.initModality(Modality.APPLICATION_MODAL);

            dialog.setTitle("Provide credentials to keys file");
            VBox root = new VBox();
            root.setPadding(new Insets(10, 10, 0, 10));

            HBox usernameBox = new HBox();
            usernameBox.setPadding(new Insets(0, 0, 2, 0));
            Label usernameLabel = new Label("Username: ");
            TextField usernameField = new TextField();
            usernameBox.getChildren().addAll(usernameLabel, usernameField);

            HBox passwordBox = new HBox();
            passwordBox.setPadding(new Insets(0, 0, 5, 0));
            Label passwordLabel = new Label("Password:  ");
            PasswordField passwordField = new PasswordField();
            passwordBox.getChildren().addAll(passwordLabel, passwordField);

            Button okButton = new Button("Ok");
            okButton.setOnAction(event -> {
                consumer.apply(usernameField.getText(), passwordField.getText());
                dialog.close();
            });
            HBox.setHgrow(okButton, Priority.ALWAYS);
            okButton.setMaxWidth(Double.MAX_VALUE);

            root.getChildren().addAll(usernameBox, passwordBox, okButton);
            dialog.setScene(new Scene(root, 270, 100));

            dialog.showAndWait();
        });
    }

}
