package com.example.cryptochat.client;

import com.google.gson.Gson;

import java.util.function.Consumer;
import java.util.function.Predicate;

public abstract class MessageConsumer<T extends WsMessage> {

    protected final String messageType;
    protected final Class<T> messageClass;
    protected Gson gson = new Gson();

    protected MessageConsumer(String messageType, Class<T> messageClass) {
        this.messageType = messageType;
        this.messageClass = messageClass;
    }

    public static <U extends WsMessage> MessageConsumer<U> permanent(String type, Class<U> messageClass, Consumer<U> consumer) {
        return new MessageConsumer<U>(type, messageClass) {

            @Override
            public boolean supports(U message) {
                return true;
            }

            @Override
            protected void accept(U message) {
                consumer.accept(message);
            }

        };
    }

    public static <U extends WsMessage> MessageConsumer<U> disposable(String type, Class<U> messageClass, Predicate<U> supports, Consumer<U> consumer) {
        return new MessageConsumer<U>(type, messageClass) {

            private boolean fulfilled = false;

            @Override
            public boolean supports(U message) {
                return supports.test(message);
            }

            @Override
            protected void accept(U message) {
                fulfilled = true;
                consumer.accept(message);
            }

            @Override
            public boolean isFulfilled() {
                return fulfilled;
            }

        };
    }

    public boolean supportsType(String type) {
        return messageType.equals(type);
    }

    public abstract boolean supports(T message);

    public void acceptJson(String jsonMessage) {
        T message = parse(jsonMessage);
        if (supports(message)) {
            this.accept(message);
        }
    }

    protected abstract void accept(T message);

    public boolean isFulfilled() {
        return false;
    }

    protected T parse(String messageJson) {
        return gson.fromJson(messageJson, messageClass);
    }

}
