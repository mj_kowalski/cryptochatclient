package com.example.cryptochat.client;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class WsMessageDispatcher {

    private final Gson gson = new Gson();
    private final Set<MessageConsumer<?>> consumers = ConcurrentHashMap.newKeySet();

    public void register(MessageConsumer<?> consumer) {
        consumers.add(consumer);
    }

    public void dispatch(String messageJson) {
        log.info("Dispatching JSON message: '{}'", messageJson);
        WsTypedMessage typedMessage = gson.fromJson(messageJson, WsTypedMessage.class);
        consumers.stream()
                .filter(consumer -> consumer.supportsType(typedMessage.getType()))
                .forEach(consumer -> consumer.acceptJson(messageJson));
        consumers.removeIf(MessageConsumer::isFulfilled);
    }

}
