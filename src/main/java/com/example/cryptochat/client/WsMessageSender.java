package com.example.cryptochat.client;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.websocket.Session;
import java.io.IOException;
import java.util.function.Consumer;
import java.util.function.Supplier;

@Slf4j
@RequiredArgsConstructor
public class WsMessageSender {

    private final Supplier<Session> sessionSupplier;
    private final Gson gson = new Gson();

    public void send(WsMessage message, Consumer<Exception> onError) {
        try {
            String messageJson = gson.toJson(message);
            log.debug("Sending message: {}", messageJson);
            sessionSupplier.get().getBasicRemote().sendText(messageJson);
        } catch (IOException e) {
            onError.accept(e);
        }
    }

}
