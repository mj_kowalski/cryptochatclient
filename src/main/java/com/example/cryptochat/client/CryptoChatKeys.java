package com.example.cryptochat.client;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.abstractj.kalium.keys.PrivateKey;
import org.abstractj.kalium.keys.PublicKey;

@Data
@AllArgsConstructor
public class CryptoChatKeys {

    private PrivateKey privateKey;

    private PublicKey publicKey;

}
