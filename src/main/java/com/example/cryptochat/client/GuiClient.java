package com.example.cryptochat.client;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;

import javax.websocket.*;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Consumer;

@Slf4j
@ClientEndpoint
public class GuiClient extends Application {

    private WsMessageDispatcher messageDispatcher = new WsMessageDispatcher();
    private MessageRenderer messageRenderer = new MessageRenderer();
    private ConversationStore conversationStore = new ConversationStore();
    private CryptoService cryptoService = new CryptoService();
    private UserKeysFileManager userKeysFileManager = new UserKeysFileManager(cryptoService);
    private Session session;
    private WsMessageSender messageSender = new WsMessageSender(() -> session);
    private ObservableList<String> userList = FXCollections.observableArrayList();
    private CryptoChatKeys myKeys;
    private String myUsername;
    private String selectedUser;

    @FXML
    private Label statusLabel;
    @FXML
    private TextFlow messageTextFlow;
    @FXML
    private Label conversationUserLabel;
    @FXML
    private Label conversationTagLabel;
    @FXML
    private ListView<String> userListView;
    @FXML
    private TextField messageTextField;
    @FXML
    private Button sendMessageButton;
    @FXML
    private Label userNameLabel;
    private Stage parentStage;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        final Parent root;
        try {
            root = FXMLLoader.load(getClass().getResource("GuiClient.fxml"));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        Scene scene = new Scene(root, 800, 600);

        parentStage = primaryStage;
        primaryStage.setTitle("CryptoChat - client");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @FXML
    private void initialize() {
        registerMessageConsumers();

        userListView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null) {
                return;
            }
            log.debug("Changing conversation user {} -> {}", oldValue, newValue);
            changeSelectedUser(newValue);
        });
        userListView.setItems(userList);

        UserAndPasswordDialogs.show(parentStage, (username, password) -> {
            log.trace("user {} fileContent {}", username, password);

            if (userKeysFileManager.exists(username)) {
                log.info("User's key file exists");
                myKeys = userKeysFileManager.read(username, password);
            } else {
                myKeys = userKeysFileManager.create(username, password);
            }
            myUsername = username;
            userNameLabel.setText("User: " + myUsername);
        });

        markDisconnected();
    }

    @FXML
    private void onDisconnectButtonClicked() {
        disconnectWebSocket();
    }

    @FXML
    private void onMessageTextKeyPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            sendMessageButton.fire();
        }
    }

    @FXML
    private void onSendButtonClicked() {
        final String messageContent = messageTextField.getText();
        if (Objects.isNull(messageContent) || messageContent.isEmpty()) {
            return;
        }
        if (selectedUser == null) {
            log.warn("The user to converse with has not been selected - send action disabled");
            ModalDialogs.info(parentStage, "Please select a user first");
            return;
        }
        if (!userList.contains(selectedUser)) {
            log.warn("User {} has disconnected - send action disabled", selectedUser);
            ModalDialogs.info(parentStage, "User " + selectedUser + " disconnected");
            return;
        }

        final String encryptedContent = cryptoService.encrypt(conversationStore.getPublicKey(selectedUser), myKeys.getPrivateKey(), messageContent);
        WsMessageOutRequest outMessage = WsMessageOutRequest.builder()
                .messageId(UUID.randomUUID().toString())
                .date(new Date())
                .to(selectedUser)
                .messageContent(encryptedContent)
                .build();
        messageSender.send(outMessage, e -> log.error("Error while sending message", e));

        messageTextField.clear();

        ConversationMessage conversationMessage = new ConversationMessage(outMessage.getDate(), myUsername, messageContent);
        conversationStore.send(outMessage.getTo(), conversationMessage);
        messageTextFlow.getChildren().addAll(messageRenderer.render(conversationMessage, myUsername));
    }

    private void changeSelectedUser(String user) {
        this.selectedUser = user;
        conversationUserLabel.setText("Conversation with " + user);

        if (!conversationStore.exists(user)) {
            requestUserPublicKey(user, publicKey -> log.info("Got public key for user '{}': '{}'", user, publicKey));
        } else {
            jumpToConversation(user);
        }
    }

    private void requestUserPublicKey(String user, Consumer<String> callback) {
        final String messageId = UUID.randomUUID().toString();
        messageDispatcher.register(
                MessageConsumer.disposable(
                        WsPublicKeyResponse.TYPE,
                        WsPublicKeyResponse.class,
                        msg -> messageId.equals(msg.getMessageId()),
                        msg -> {
                            log.info("Received public key for user '{}' ({})", user, msg.getPublicKey());
                            beginConversation(user, msg.getPublicKey());
                            callback.accept(msg.getPublicKey());
                        }
                )
        );
        messageSender.send(
                WsPublicKeyRequest.builder().messageId(messageId).user(user).build(),
                e -> log.error("Error while requesting user public key", e)
        );
    }

    private void beginConversation(String user, String publicKey) {
        Platform.runLater(() -> {
            final String tag = calculateConversationTag(publicKey);
            conversationStore.begin(user, cryptoService.readPublicKey(publicKey), tag);
            messageTextFlow.getChildren().clear();
            presentConversationTag(tag);
        });
    }

    private void presentConversationTag(String tag) {
        conversationTagLabel.setText("(" + tag + ")");
    }

    private String calculateConversationTag(String publicKey) {
        String myPublicKey = myKeys.getPublicKey().toString();
        byte[] publicKeyBytes = publicKey.getBytes(StandardCharsets.UTF_8);
        byte[] myPublicKeyBytes = myPublicKey.getBytes(StandardCharsets.UTF_8);
        byte[] tagBytes = new byte[myPublicKeyBytes.length];
        for (int i = 0; i < myPublicKeyBytes.length; ++i) {
            int xor = (myPublicKeyBytes[i] ^ publicKeyBytes[i]);
            tagBytes[i] = (byte) (0xff & xor);
        }
        return cryptoService.calculateHash(tagBytes);
    }

    private void jumpToConversation(String user) {
        // TODO: optimize?
        Platform.runLater(() -> {
            selectedUser = user;
            presentConversationTag(conversationStore.getTag(user));
            messageTextFlow.getChildren().clear();
            conversationStore.getMessages(user).forEach(
                    message -> messageTextFlow.getChildren().addAll(messageRenderer.render(message, myUsername))
            );
        });
    }

    private void registerMessageConsumers() {
        messageDispatcher.register(MessageConsumer.permanent(WsErrorResponse.TYPE, WsErrorResponse.class, msg -> {
            log.warn("Error message received: {}", msg);
            ModalDialogs.error(parentStage, msg.getMessage());
        }));
        messageDispatcher.register(MessageConsumer.permanent(WsUsersUpdateResponse.TYPE, WsUsersUpdateResponse.class, this::handleUsersUpdate));
        messageDispatcher.register(MessageConsumer.permanent(WsMessageInResponse.TYPE, WsMessageInResponse.class, this::handleIncomingMessage));
    }

    @FXML
    private void onLoginButtonClicked() {
        messageSender.send(
                WsLoginRequest.builder().user(myUsername).publicKey(myKeys.getPublicKey().toString()).build(),
                e -> log.error("Login attempt failed", e)
        );
    }

    private void disconnectWebSocket() {
        try {
            session.close();
        } catch (IOException e) {
            log.error("Error while disconnecting from WS", e);
            throw new IllegalStateException(e);
        }
    }

    @OnOpen
    public void onOpen(Session session) {
        log.info("WS connection opened");
        this.session = session;
        Platform.runLater(this::markConnected);
    }

    @OnMessage
    public void onMessage(String message) {
        log.info("WS message received: '{}'", message);
        messageDispatcher.dispatch(message);
    }

    @OnClose
    public void onClose() {
        log.info("Closing WS connection");
        Platform.runLater(this::markDisconnected);
    }

    private void handleUsersUpdate(WsUsersUpdateResponse updateResponse) {
        log.info("Updating users list");
        Platform.runLater(() -> userList.setAll(updateResponse.getUsers()));
    }

    private void handleIncomingMessage(WsMessageInResponse incomingMessage) {
        log.debug("Handling incoming message: {}", incomingMessage);
        final String from = incomingMessage.getFrom();
        if (conversationStore.exists(from)) {
            String messageContent = cryptoService.decrypt(
                    myKeys.getPrivateKey(),
                    conversationStore.getPublicKey(from),
                    incomingMessage.getMessageContent());
            conversationStore.receive(from, new ConversationMessage(incomingMessage.getDate(), from, messageContent));
            Platform.runLater(() -> changeSelectedUser(from));
        } else {
            requestUserPublicKey(from, publicKey -> {
                conversationStore.begin(from, cryptoService.readPublicKey(publicKey), calculateConversationTag(publicKey));
                String messageContent = cryptoService.decrypt(
                        myKeys.getPrivateKey(),
                        conversationStore.getPublicKey(from),
                        incomingMessage.getMessageContent());
                conversationStore.receive(from, new ConversationMessage(incomingMessage.getDate(), from, messageContent));
                Platform.runLater(() -> changeSelectedUser(from));
            });
        }
    }

    private void markConnected() {
        statusLabel.setText("connected");
        statusLabel.setTextFill(Color.GREEN);
    }

    private void markDisconnected() {
        statusLabel.setText("disconnected");
        statusLabel.setTextFill(Color.RED);
    }

    @FXML
    private void onConnectButtonClicked() {
        WebSocketContainer container = ContainerProvider.getWebSocketContainer();
        try {
            URI uri = URI.create("ws://cryptochat.gq:8080/ws");
            container.connectToServer(this, uri);
        } catch (DeploymentException | IOException ex) {
            log.error("Connecting to server failed", ex);
        }
    }

}
