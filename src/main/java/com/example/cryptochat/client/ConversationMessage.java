package com.example.cryptochat.client;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class ConversationMessage {

    private Date date;

    private String from;

    private String content;

}
