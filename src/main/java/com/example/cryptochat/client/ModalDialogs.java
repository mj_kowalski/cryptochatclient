package com.example.cryptochat.client;

import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ModalDialogs {

    private ModalDialogs() {
        // static only
    }

    public static void info(Stage parentStage, String message) {
        show(parentStage, "Information", message);
    }

    public static void error(Stage parentStage, String message) {
        show(parentStage, "Error", message);
    }

    private static void show(Stage parentStage, String title, String message) {
        Platform.runLater(() -> {
            Stage dialog = new Stage();
            dialog.initOwner(parentStage);
            dialog.initModality(Modality.APPLICATION_MODAL);

            dialog.setTitle(title);
            StackPane root = new StackPane();
            root.getChildren().add(new Text(message));
            dialog.setScene(new Scene(root, 250, 100));

            dialog.showAndWait();
        });
    }

}
