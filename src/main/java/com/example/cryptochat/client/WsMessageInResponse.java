package com.example.cryptochat.client;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class WsMessageInResponse extends WsTypedMessage {

    public static final String TYPE = "MESSAGE_IN";

    public WsMessageInResponse() {
        this.type = TYPE;
    }

    public WsMessageInResponse(String messageId, Date date, String from, String messageContent) {
        this();
        this.messageId = messageId;
        this.date = date;
        this.from = from;
        this.messageContent = messageContent;
    }

    private String messageId;

    private Date date;

    private String from;

    private String messageContent;

}
