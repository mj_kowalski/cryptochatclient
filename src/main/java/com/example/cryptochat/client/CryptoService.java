package com.example.cryptochat.client;

import lombok.extern.slf4j.Slf4j;
import org.abstractj.kalium.NaCl;
import org.abstractj.kalium.crypto.*;
import org.abstractj.kalium.keys.KeyPair;
import org.abstractj.kalium.keys.PrivateKey;
import org.abstractj.kalium.keys.PublicKey;

import java.nio.charset.StandardCharsets;

import static org.abstractj.kalium.encoders.Encoder.HEX;

@Slf4j
public class CryptoService {

    public CryptoChatKeys generateKeys() {
        KeyPair keyPair = new KeyPair();
        return new CryptoChatKeys(keyPair.getPrivateKey(), keyPair.getPublicKey());
    }

    public PublicKey readPublicKey(String publicKeyStr) {
        return new PublicKey(HEX.decode(publicKeyStr));
    }

    public CryptoChatKeys readKeys(String privateKeyStr, String publicKeyStr) {
        PublicKey publicKey = readPublicKey(publicKeyStr);
        PrivateKey privateKey = new PrivateKey(HEX.decode(privateKeyStr));
        return new CryptoChatKeys(privateKey, publicKey);
    }

    /**
     * Encrypts string using symmetric secret key.
     * https://download.libsodium.org/doc/secret-key_cryptography/authenticated_encryption.html
     *
     * @param key     Secret key (keep it private!)
     * @param content Content to encrypt
     */
    public String encryptSymmetric(String key, String content) {
        byte[] nonce = generateSecretBoxNonce();
        byte[] encrypted = new SecretBox(prepareSecretBoxKey(key)).encrypt(nonce, bytesOf(content));
        return HEX.encode(nonce) + ":" + HEX.encode(encrypted);
    }

    /**
     * Decrypts string encrypted with symmetric secret key
     * https://download.libsodium.org/doc/secret-key_cryptography/authenticated_encryption.html
     *
     * @param key              Secret key (keep it private)
     * @param encryptedContent Encrypted content (to be decrypted)
     */
    public String decryptSymmetric(String key, String encryptedContent) {
        String[] split = encryptedContent.split(":");
        byte[] nonce = HEX.decode(split[0]);
        byte[] encryptedData = HEX.decode(split[1]);
        byte[] decrypted = new SecretBox(prepareSecretBoxKey(key)).decrypt(nonce, encryptedData);
        return toUtf8String(decrypted);
    }

    /**
     * Encrypts string using asymmetric keys (authenticated encryption)
     * https://download.libsodium.org/doc/public-key_cryptography/authenticated_encryption.html
     *
     * @param receiverPublicKey Receiver's public key
     * @param senderPrivateKey  Sender's private key
     * @param text              Content to encrypt
     */
    public String encrypt(PublicKey receiverPublicKey, PrivateKey senderPrivateKey, String text) {
        Box senderBox = new Box(receiverPublicKey, senderPrivateKey);
        byte[] nonceBytes = generateCryptoBoxNonce();
        byte[] encrypted = senderBox.encrypt(nonceBytes, bytesOf(text));
        final String result = HEX.encode(nonceBytes) + ":" + HEX.encode(encrypted);
        log.debug("Encryption result for: '{}': '{}'", text, result);
        return result;
    }

    /**
     * Decrypts string encrypted with asymmetric keys (authenticated encryption)
     * https://download.libsodium.org/doc/public-key_cryptography/authenticated_encryption.html
     *
     * @param receiverPrivateKey Receiver's private key
     * @param senderPublicKey    Sender's public key
     * @param encryptedText      Encrypted content (to be decrypted)
     */
    public String decrypt(PrivateKey receiverPrivateKey, PublicKey senderPublicKey, String encryptedText) {
        String[] split = encryptedText.split(":");
        Box receiverBox = new Box(senderPublicKey, receiverPrivateKey);
        byte[] decrypted = receiverBox.decrypt(split[0], split[1], HEX);
        final String result = toUtf8String(decrypted);
        log.debug("Decryption result for '{}': '{}'", encryptedText, result);
        return result;
    }

    /**
     * Calculates hash of data using BLAKE2b algorithm
     * https://download.libsodium.org/doc/hashing/generic_hashing.html
     *
     * @param data Input to hash function
     */
    public String calculateHash(byte[] data) {
        final String hash = HEX.encode(new Hash().blake2(data));
        log.debug("Generated hash for '{}': '{}'", data, hash);
        return hash;
    }

    /**
     * Generates random nonce for CryptoBox.
     */
    private byte[] generateCryptoBoxNonce() {
        return new Random().randomBytes(NaCl.Sodium.CRYPTO_BOX_CURVE25519XSALSA20POLY1305_NONCEBYTES);
    }

    /**
     * Generates random nonce for SecretBox.
     */
    private byte[] generateSecretBoxNonce() {
        return new Random().randomBytes(NaCl.Sodium.CRYPTO_SECRETBOX_XSALSA20POLY1305_NONCEBYTES);
    }

    /**
     * Transforms user specified key to valid form (expected length).
     */
    byte[] prepareSecretBoxKey(String key) {
        int expectedKeyLength = NaCl.Sodium.CRYPTO_SECRETBOX_XSALSA20POLY1305_KEYBYTES;
        byte[] keyBytes = bytesOf(key);
        if (expectedKeyLength > keyBytes.length) {
            return Util.prependZeros(expectedKeyLength - keyBytes.length, keyBytes);
        } else if (expectedKeyLength < keyBytes.length) {
            return Util.removeZeros(keyBytes.length - expectedKeyLength, keyBytes);
        } else {
            return keyBytes;
        }
    }

    byte[] bytesOf(String utf8String) {
        return utf8String.getBytes(StandardCharsets.UTF_8);
    }

    String toUtf8String(byte[] rawBytes) {
        return new String(rawBytes, StandardCharsets.UTF_8);
    }

}
