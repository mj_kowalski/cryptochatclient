package com.example.cryptochat.client;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WsPublicKeyRequest extends WsTypedMessage {

    public static final String TYPE = "PUBLIC_KEY_REQUEST";

    public WsPublicKeyRequest() {
        this.type = TYPE;
    }

    public WsPublicKeyRequest(String messageId, String user) {
        this();
        this.messageId = messageId;
        this.user = user;
    }

    private String messageId;

    private String user;

}
