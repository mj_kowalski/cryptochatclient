package com.example.cryptochat.client;

public interface UserKeysFileConsumer {

    void apply(String usernameTextFieldText, String password);

}
