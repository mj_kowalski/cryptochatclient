package com.example.cryptochat.client;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public class WsTypedMessage implements WsMessage {

    @Getter
    protected String type;

}
