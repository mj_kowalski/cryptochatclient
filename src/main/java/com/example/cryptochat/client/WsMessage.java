package com.example.cryptochat.client;

public interface WsMessage {

    String getType();

}
