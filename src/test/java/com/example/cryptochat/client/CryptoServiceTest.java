package com.example.cryptochat.client;

import org.abstractj.kalium.NaCl;
import org.abstractj.kalium.crypto.Random;
import org.abstractj.kalium.keys.KeyPair;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CryptoServiceTest {

    private static final int SECRET_BOX_EXPECTED_KEY_LENGTH = NaCl.Sodium.CRYPTO_SECRETBOX_XSALSA20POLY1305_KEYBYTES;
    private CryptoService service = new CryptoService();

    @Test
    public void shouldDecryptSymmetricEncryptedMessage() {
        // given
        final String secret = "Top secret message";
        final String key = "ExampleEncryptionKey";
        // when
        String encrypted = service.encryptSymmetric(key, secret);
        String decrypted = service.decryptSymmetric(key, encrypted);
        // then
        assertThat(decrypted).isEqualTo(secret);
    }

    @Test
    public void shouldNotRepeatSymmetricEncryptionResultForSameMessage() {
        // given
        final String secret = "Top secret message";
        final String key = "ExampleEncryptionKey";
        // when
        String encrypted1 = service.encryptSymmetric(key, secret);
        String encrypted2 = service.encryptSymmetric(key, secret);
        // then
        assertThat(encrypted1).isNotEqualTo(encrypted2);
    }

    @Test
    public void shouldDecryptEncryptedMessage() {
        // given
        final String secretMessageToAlice = "Such secret. Much secure. Wow";
        KeyPair aliceKeyPair = new KeyPair(new Random().randomBytes());
        KeyPair bobKeyPair = new KeyPair(new Random().randomBytes());
        // when
        final String encrypted =
                service.encrypt(aliceKeyPair.getPublicKey(), bobKeyPair.getPrivateKey(), secretMessageToAlice);
        final String decrypted =
                service.decrypt(aliceKeyPair.getPrivateKey(), bobKeyPair.getPublicKey(), encrypted);
        // then
        assertThat(decrypted).isEqualTo(secretMessageToAlice);
    }

    @Test
    public void shouldNotRepeatEncryptionResultForSameMessage() {
        // given
        final String secretMessageToAlice = "Such secret. Much secure. Wow";
        KeyPair aliceKeyPair = new KeyPair(new Random().randomBytes());
        KeyPair bobKeyPair = new KeyPair(new Random().randomBytes());
        // when
        final String encrypted1 =
                service.encrypt(aliceKeyPair.getPublicKey(), bobKeyPair.getPrivateKey(), secretMessageToAlice);
        final String decrypted2 =
                service.encrypt(aliceKeyPair.getPublicKey(), bobKeyPair.getPrivateKey(), secretMessageToAlice);
        // then
        assertThat(encrypted1).isNotEqualTo(decrypted2);
    }

    @Test
    public void shouldGenerateSameHashForSameInput() {
        // given
        final String message = "top secret";
        // when
        final String hash1 = service.calculateHash(service.bytesOf(message));
        final String hash2 = service.calculateHash(service.bytesOf(message));
        // then
        assertThat(hash1).isEqualTo(hash2);
    }

    @Test
    public void shouldGenerateDifferentHashForDifferentInput() {
        // given
        final String originalMessage = "top secret";
        final String modifiedMessage = originalMessage + "!";
        // when
        final String originalMessageHash = service.calculateHash(service.bytesOf(originalMessage));
        final String modifiedMessageHash = service.calculateHash(service.bytesOf(modifiedMessage));
        // then
        assertThat(originalMessageHash).isNotEqualTo(modifiedMessageHash);
    }

    @Test
    public void shouldExtendShortSecretBoxKey() {
        // given
        final String key = "theKey";
        assertThat(service.bytesOf(key).length).isLessThan(SECRET_BOX_EXPECTED_KEY_LENGTH);
        // when
        byte[] result = service.prepareSecretBoxKey(key);
        // then
        assertThat(result.length).isEqualTo(SECRET_BOX_EXPECTED_KEY_LENGTH);
    }

    @Test
    public void shouldNotModifySecretBoxKeyOfValidLength() {
        // given
        final String key = "thisIsTheOnlyKeyOfValidLength!!!";
        assertThat(service.bytesOf(key).length).isEqualTo(SECRET_BOX_EXPECTED_KEY_LENGTH);
        // when
        byte[] result = service.prepareSecretBoxKey(key);
        // then
        assertThat(result.length).isEqualTo(SECRET_BOX_EXPECTED_KEY_LENGTH);
    }

    @Test
    public void shouldShortenTooLongSecretBoxKey() {
        // given
        final String key = "thisIsReallyNotTheOnlyOneKeyYouAreLookingFor";
        assertThat(service.bytesOf(key).length).isGreaterThan(SECRET_BOX_EXPECTED_KEY_LENGTH);
        // when
        byte[] result = service.prepareSecretBoxKey(key);
        // then
        assertThat(result.length).isEqualTo(SECRET_BOX_EXPECTED_KEY_LENGTH);
    }


}