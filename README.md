# CryptoChat Client

This is a JavaFX GUI client for https://bitbucket.org/marcin_m/cryptochatserver - simple communicator with end-to-end encryption support.

_Disclaimer:_ this project should not be considered as a full-featured secure communicator! This is just a prototype created as a showcase for [libsodium](https://download.libsodium.org/doc/) library (via it's Java wrapper [kalium](https://github.com/abstractj/kalium)).


## Requirements

* Java 8 SDK
* Maven
* libsodium

## Running

_Prerequisite:_ [server](https://bitbucket.org/marcin_m/cryptochatserver) instance must be up and running.

```
# clone repository
git clone https://bitbucket.org/mj_kowalski/cryptochatclient.git
# build JAR archive
mvn clean package
# run client
java -jar java -jar target/cryptochatclient.jar
```

## Manual

To begin the conversation do the following:

* connect to the server
* log in as a selected user
* choose one from the list of available users
* have fun :-)